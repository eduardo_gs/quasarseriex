# QUASAR X - SOLENTECH'S RFID READER

API examples for the QUASAR X RFID reader using Python 3.

Reader firmware version: 1.0.4. It may work on previous versions, with some bugs.

** Developed for the UDP/IP reader only! **
