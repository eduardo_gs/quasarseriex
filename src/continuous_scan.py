import socket, time, math, sys, signal, os, platform

local_ip = "192.168.5.2"
reader_ip = "192.168.5.10"
port = 8080
mtu = 1500
timeout = 3

def signal_handler(signal, frame):
    bytes_to_send = str.encode("stop continuous scan")
    server_addr_port = (reader_ip, port)
    buf_size = mtu
    udp_client = socket.socket(family=socket.AF_INET, type=socket.SOCK_DGRAM)
    udp_client.settimeout(timeout)
    udp_client.sendto(bytes_to_send, server_addr_port)
    message_from_reader = udp_client.recvfrom(buf_size)
    if message_from_reader[0][0] == 0 and message_from_reader[0][0] == 0:
        print("Continuous tag scanning successfully completed.")
        udp_client.close()
        sys.exit(0)
    else:
        print("Continuous tag scanning ended with error.")
        udp_client.close()
        sys.exit(-1)

def select_option():
    print("Continuous tag scan")
    print("(1) PC + EPC")
    print("(2) PC + EPC + RSSI")
    print("(3) PC + EPC + TID")
    option = int(input())
    if option == 1:
        return 1, "start continuous scan"
    elif option == 2:
        return 2, "start rssi continuous scan"
    elif option == 3:
        return 3, "start tid continuous scan"
    else:
        exit(-1)

def main():
    max_tags = 8196
    num_bytes_pc = 2
    num_bytes_epc = 32
    num_bytes_rssi = 4
    num_bytes_tid = 16
    pc = [[0 for x in range(num_bytes_pc)] for y in range(max_tags)]
    epc = [[0 for x in range(num_bytes_epc)] for y in range(max_tags)]
    raw_rssi = [[0 for x in range(num_bytes_rssi)] for y in range(max_tags)]
    rssi = [[0 for x in range(max_tags)]]
    tid = [[0 for x in range(num_bytes_tid)] for y in range(max_tags)]
    pc_str = ["" for y in range(max_tags)]
    epclen_str = ["" for y in range(max_tags)]
    epc_str = ["" for y in range(max_tags)]
    rssi_str = ["" for y in range(max_tags)]
    tid_str = ["" for y in range(max_tags)]
    list_pc = ["" for y in range(max_tags)]
    list_epc = ["" for y in range(max_tags)]
    list_rssi = ["" for y in range(max_tags)]
    list_tid = ["" for y in range(max_tags)]
    list_count = [0 for y in range(max_tags)]
    qty_tags_list = 0
    command, message_to_reader = select_option()
    bytes_to_send = str.encode(message_to_reader)
    server_addr_port = (reader_ip, port)
    buf_size = mtu
    udp_client = socket.socket(family=socket.AF_INET, type=socket.SOCK_DGRAM)
    udp_client.settimeout(None)
    udp_client.bind((local_ip, port))
    udp_client.sendto(bytes_to_send, server_addr_port)
    print("Requested to reader: " + message_to_reader)  
    while(True):
        message_from_reader = udp_client.recvfrom(buf_size)        
        reader_time_epoch = message_from_reader[0][2] << 24 | message_from_reader[0][3] << 16 \
                            | message_from_reader[0][4] << 8 | message_from_reader[0][5]
        reader_time_ms = message_from_reader[0][6] << 8 | message_from_reader[0][7]
        local_time = time.localtime(reader_time_epoch)
        if platform.system() == "Linux":
            os.system("clear") # linux
        elif platform.system() == "Windows":
            os.system("cls") # windows
        else: # not implemented for MAC
            pass
        print("Reader time: " + "{:04d}".format(local_time.tm_year) + "/" + \
            "{:02d}".format(local_time.tm_mon) + "/" + "{:02d}".format(local_time.tm_mday) + \
            " - " + "{:02d}".format(local_time.tm_hour) + ":" + "{:02d}".format(local_time.tm_min) + \
            ":" + "{:02d}".format(local_time.tm_sec) + ":" + "{:03d}".format(reader_time_ms))
        if message_from_reader[0][0] == 0 and message_from_reader[0][0] == 0:
            num_tags = message_from_reader[0][8]
            inc = 0
            if command == 1:
                for i in range(0, num_tags):
                    tmp_pc = ""
                    tmp_epc = ""
                    epc_len = (message_from_reader[0][11 + inc] >> 2)
                    pc[i][0] = message_from_reader[0][11 + inc]
                    pc[i][1] = message_from_reader[0][12 + inc]
                    pc_str[i] = "{:02x}{:02x}".format(pc[i][0], pc[i][1])
                    epclen_str[i] = "{:02d}".format(epc_len)
                    for j in range(0, epc_len):
                        epc[i][j] = message_from_reader[0][(13 + inc + j)]
                        tmp_epc = tmp_epc + "{:02x}".format(epc[i][j])
                    epc_str[i] = tmp_epc
                    inc = inc + 2 + 32
                i = 0
                j = 0
                for i in range(num_tags):
                    for j in range(qty_tags_list):
                        if list_epc[j] == epc_str[i]:
                            result = True
                        else:
                            result = False
                        if result == True:
                            list_count[j] = list_count[j] + 1
                            break
                        else:
                            j = j + 1
                    if j == qty_tags_list and qty_tags_list < max_tags:
                        list_pc[j] = pc_str[i]
                        list_epc[j] = epc_str[i]
                        list_count[j] = list_count[j] + 1
                        qty_tags_list = qty_tags_list + 1
                print(str(qty_tags_list) + " inventoried tags.")
                for i in range(qty_tags_list):
                    print("pc -> " + list_pc[i] + " epc -> " + list_epc[i] + " count -> " + str(list_count[i]))

            elif command == 2:
                for i in range(0, num_tags):
                    tmp_epc = ""
                    epc_len = (message_from_reader[0][11 + inc] >> 2)
                    pc[i][0] = message_from_reader[0][11 + inc]
                    pc[i][1] = message_from_reader[0][12 + inc]
                    tmp_pc = "{:02x}{:02x}".format(pc[i][0], pc[i][1])
                    pc_str[i] = tmp_pc
                    for j in range(0, epc_len):
                        epc[i][j] = message_from_reader[0][(13 + inc + j)]
                        tmp_epc = tmp_epc + "{:02x}".format(epc[i][j])
                        epc_str[i] = tmp_epc           
                    raw_rssi[i][0] = message_from_reader[0][(45 + inc)]
                    raw_rssi[i][1] = message_from_reader[0][(46 + inc)]
                    raw_rssi[i][2] = message_from_reader[0][(47 + inc)]
                    raw_rssi[i][3] = message_from_reader[0][(48 + inc)]              
                    # rssi_i = 20 * math.log10(raw_rssi[i][0]) - raw_rssi[i][2] - 63
                    # rssi_q = 20 * math.log10(raw_rssi[i][1]) - raw_rssi[i][3] - 63
                    # rfin_i = 10 ** (rssi_i/20)
                    # rfin_q = 10 ** (rssi_q/20)
                    # rfin = math.sqrt(rfin_i**2 + rfin_q**2)
                    # rssi = 20 * math.log10(rfin)
                    # tmp_rssi = "{:02.2f}".format(rssi)
                    # -> changed to prevent log(0)
                    gain_i_l = 10**((raw_rssi[i][2] + 63)/20)
                    gain_q_l = 10**((raw_rssi[i][3] + 63)/20)
                    rfin_i = raw_rssi[i][0]/gain_i_l
                    rfin_q = raw_rssi[i][1]/gain_q_l
                    rfin = math.sqrt(rfin_i**2 + rfin_q**2)
                    rssi = 20 * math.log10(rfin)
                    tmp_rssi = "{:02.2f}".format(rssi)
                    rssi_str[i] = tmp_rssi
                    inc = inc + 2 + 32 + 4
                i = 0
                j = 0
                for i in range(0, num_tags):
                    for j in range(0, qty_tags_list):
                        if list_epc[j] == epc_str[i]:
                            result = True
                        else:
                            result = False
                        if result == True:
                            list_rssi[j] = rssi_str[i]
                            list_count[j] = list_count[j] + 1
                            break
                        else:
                            j = j + 1
                    if j == qty_tags_list and qty_tags_list < max_tags:
                        list_pc[j] = pc_str[i]
                        list_epc[j] = epc_str[i]
                        list_rssi[j] = rssi_str[i]
                        list_count[j] = list_count[j] + 1
                        qty_tags_list = qty_tags_list + 1
                print(str(qty_tags_list) + " inventoried tags.")
                for i in range(0, qty_tags_list):
                    print("pc -> " + list_pc[i] + " epc -> " + list_epc[i] + " rssi -> " + list_rssi[i] + " count -> " + str(list_count[i]))

            elif command == 3:
                for i in range(0, num_tags):
                    tmp_epc = ""
                    tmp_tid = ""
                    tid_len = message_from_reader[0][11 + inc]
                    epc_len = (message_from_reader[0][12 + inc] >> 2)
                    pc[i][0] = message_from_reader[0][12 + inc]
                    pc[i][1] = message_from_reader[0][13 + inc]
                    tmp_pc = "{:02x}{:02x}".format(pc[i][0], pc[i][1])
                    pc_str[i] = tmp_pc
                    for j in range(0, epc_len):
                        epc[i][j] = message_from_reader[0][(14 + inc + j)]
                        tmp_epc = tmp_epc + "{:02x}".format(epc[i][j])
                    epc_str[i] = tmp_epc
                    for j in range(0, tid_len):
                        tid[i][j] = message_from_reader[0][(46 + inc + j)]
                        tmp_tid = tmp_tid + "{:02x}".format(tid[i][j])
                    tid_str[i] = tmp_tid    
                    inc = inc + 1 + 2 + 32 + 16
                i = 0
                j = 0
                for i in range(0, num_tags):
                    for j in range(0, qty_tags_list):
                        if list_epc[j] == epc_str[i] and list_tid[j] == tid_str[i]:
                            result = True
                        else:
                            result = False
                        if result == True:
                            list_count[j] = list_count[j] + 1
                            break
                        else:
                            j = j + 1
                    if j == qty_tags_list and qty_tags_list < max_tags:
                        list_pc[j] = pc_str[i]
                        list_epc[j] = epc_str[i]
                        list_tid[j] = tid_str[i]
                        list_count[j] = list_count[j] + 1
                        qty_tags_list = qty_tags_list + 1
                print(str(qty_tags_list) + " inventoried tags.")
                for i in range(0, qty_tags_list):
                    print("pc -> " + list_pc[i] + " epc -> " + list_epc[i] + " tid -> " + list_tid[i] + " count -> " + str(list_count[i]))  
        else:
            print("\nError.")
            input()
            break
        
if __name__ == "__main__":
    signal.signal(signal.SIGINT, signal_handler)
    main()