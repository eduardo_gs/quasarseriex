import socket, time, sys, math

reader_ip = "192.168.5.10"
port = 8080
mtu = 1500
timeout = 3
first_antenna = 1
last_antenna = 2

max_tags = 8196
num_bytes_pc = 2
num_bytes_epc = 32
num_bytes_rssi = 4
num_bytes_tid = 16
pc = [[0 for x in range(num_bytes_pc)] for y in range(max_tags)]
epc = [[0 for x in range(num_bytes_epc)] for y in range(max_tags)]
raw_rssi = [[0 for x in range(num_bytes_rssi)] for y in range(max_tags)]
rssi = [[0 for x in range(max_tags)]]
tid = [[0 for x in range(num_bytes_tid)] for y in range(max_tags)]
pc_str = ["" for y in range(max_tags)]
epclen_str = ["" for y in range(max_tags)]
epc_str = ["" for y in range(max_tags)]
rssi_str = ["" for y in range(max_tags)]
tid_str = ["" for y in range(max_tags)]
list_pc = ["" for y in range(max_tags)]
list_epc = ["" for y in range(max_tags)]
list_rssi = ["" for y in range(max_tags)]
list_tid = ["" for y in range(max_tags)]
list_count = [0 for y in range(max_tags)]
qty_tags_list = 0

def handle_udp(message_to_server):
    bytes_to_send = str.encode(message_to_server)
    server_addr_port = (reader_ip, port)
    buf_size = mtu

    # Create a UDP socket at client side
    udp_client = socket.socket(family=socket.AF_INET, type=socket.SOCK_DGRAM)
    # Set socket timeout
    udp_client.settimeout(timeout)
    # Send to server using created UDP socket
    udp_client.sendto(bytes_to_send, server_addr_port)
    # Receive from server using created UDP socket
    message_from_server = udp_client.recvfrom(buf_size)
    # close the UDP socket
    udp_client.close()
    return message_from_server

def reader_time(message_from_server):
    reader_time_epoch = message_from_server[0][2] << 24 | message_from_server[0][3] << 16 \
                        |   message_from_server[0][4] << 8  | message_from_server[0][5]
    reader_time_ms = message_from_server[0][6] << 8 | message_from_server[0][7]
    local_time = time.localtime(reader_time_epoch)
    print("\nReader time: " + "{:04d}".format(local_time.tm_year) + "/" + \
        "{:02d}".format(local_time.tm_mon) + "/" + "{:02d}".format(local_time.tm_mday) + \
        " - " + "{:02d}".format(local_time.tm_hour) + ":" + "{:02d}".format(local_time.tm_min) + \
        ":" + "{:02d}".format(local_time.tm_sec) + ":" + "{:03d}".format(reader_time_ms))

def check_reader_response(message_from_server, command, num_antenna):
    qty_tags_list = 0
    if message_from_server[0][0] == 0 and message_from_server[0][0] == 0:
        if command == 0:
            print("The antenna " + "{:02d}".format(num_antenna) + " was successfully selected." )
            return 0
        elif command == 1:
            print("The operation of antenna " + "{:02d}".format(num_antenna) + " is normal.")
            return 0
        else:
            num_tags = message_from_server[0][8]
            inc = 0
            for i in range(0, num_tags):
                tmp_epc = ""
                epc_len = (message_from_server[0][11 + inc] >> 2)
                pc[i][0] = message_from_server[0][11 + inc]
                pc[i][1] = message_from_server[0][12 + inc]
                tmp_pc = "{:02x}{:02x}".format(pc[i][0], pc[i][1])
                pc_str[i] = tmp_pc
                for j in range(0, epc_len):
                    epc[i][j] = message_from_server[0][(13 + inc + j)]
                    tmp_epc = tmp_epc + "{:02x}".format(epc[i][j])
                    epc_str[i] = tmp_epc           
                raw_rssi[i][0] = message_from_server[0][(45 + inc)]
                raw_rssi[i][1] = message_from_server[0][(46 + inc)]
                raw_rssi[i][2] = message_from_server[0][(47 + inc)]
                raw_rssi[i][3] = message_from_server[0][(48 + inc)]              
                rssi_i = 20 * math.log10(raw_rssi[i][0]) - raw_rssi[i][2] - 63
                rssi_q = 20 * math.log10(raw_rssi[i][1]) - raw_rssi[i][3] - 63
                rfin_i = 10 ** (rssi_i/20)
                rfin_q = 10 ** (rssi_q/20)
                rfin = math.sqrt(rfin_i**2 + rfin_q**2)
                rssi = 20 * math.log10(rfin)
                tmp_rssi = "{:02.2f}".format(rssi)
                rssi_str[i] = tmp_rssi
                inc = inc + 2 + 32 + 4
            i = 0
            j = 0
            for i in range(0, num_tags):
                for j in range(0, qty_tags_list):
                    if list_epc[j] == epc_str[i]:
                        result = True
                    else:
                        result = False
                    if result == True:
                        list_rssi[j] = rssi_str[i]
                        list_count[j] = list_count[j] + 1
                        break
                    else:
                        j = j + 1
                if j == qty_tags_list and qty_tags_list < max_tags:
                    list_pc[j] = pc_str[i]
                    list_epc[j] = epc_str[i]
                    list_rssi[j] = rssi_str[i]
                    list_count[j] = list_count[j] + 1
                    qty_tags_list = qty_tags_list + 1
            print(str(qty_tags_list) + " inventoried tags.")
            for i in range(0, qty_tags_list):
                print("pc -> " + list_pc[i] + " epc -> " + list_epc[i] + " rssi -> " + list_rssi[i] + " count -> " + str(list_count[i]))
            return 0
    else:
        if command == 0:
            print("ERROR -> It was not possible to select antenna " + "{:02d}.".format(num_antenna))
            return -1
        elif command == 1:
            print("ERROR -> The antenna " + "{:02d}".format(num_antenna) + " was not identified.")
            return -1
        else:
            print("ERROR -> It was not possible to read tags.")
            return -1

def main():
    for i in range (first_antenna, last_antenna + 1):
        command = 0
        message_to_server = "set antenna " + "{:02d}".format(i)
        message_from_server = handle_udp(message_to_server)
        reader_time(message_from_server)
        check_reader_response(message_from_server, command, i)
        
        command = 1
        message_to_server = "check antenna"
        message_from_server = handle_udp(message_to_server)
        reader_time(message_from_server)
        check_antenna = check_reader_response(message_from_server, command, i)

        if check_antenna == 0:
            command = 2
            message_to_server = "start rssi inventory"
            message_from_server = handle_udp(message_to_server)
            reader_time(message_from_server)
            check_reader_response(message_from_server, command, i)

        
if __name__ == "__main__":
    main()