import socket, time
import binascii
# 00571588f8a238b141041180 -> dogbone
# 00000000000000000c005332 -> ceitec
def main():
    #ap[2] ul[2] epc[ul] sa[2] dl[2]
    # message_to_server = "read memory user 00000000 000c 00000000000000000c005332 0000 0000 "
    # message_to_server = "write memory user 00000000 000c 00000000000000000c005332 0000 0002 ed00ed00"
    # message_to_server = "blockwrite memory user 00000000 000c 00000000000000000c005332 0000 0002 ed922405"
    # message_to_server = "blockerase memory user 00000000 000c 00000000000000000c005332 0000 0002"
    
    # while 1:
    # message_to_server = "start artefato rssi inventory"
    # message_to_server = "config default ethernet 192.168.5.25 255.255.255.0 192.168.5.254"
    
    # message_to_server = "set gpio in 10"
    # message_to_server = "set gpio out 10"
    # message_to_server = "write gpio high 16"
    message_to_server = "write gpio low 16"
    message_to_server = "read gpio 16"
    
    print("Message to server: " + message_to_server)
    bytes_to_send = str.encode(message_to_server)
    server_addr_port = ("192.168.5.10", 8080)
    buf_size = 1500

    # Create a UDP socket at client side
    udp_client = socket.socket(family=socket.AF_INET, type=socket.SOCK_DGRAM)
    udp_client.settimeout(3)
    # udp_client.settimeout(None)
    # udp_client.bind(("192.168.5.2", 8080))
    # udp_client.sendto(bytes_to_send, server_addr_port)

    # Send to server using created UDP socket
    udp_client.sendto(bytes_to_send, server_addr_port)
    message_from_server = udp_client.recvfrom(buf_size)
    udp_client.close()

    reader_time_epoch = message_from_server[0][2] << 24 | message_from_server[0][3] << 16 \
                        | message_from_server[0][4] << 8 | message_from_server[0][5]

    reader_time_ms = message_from_server[0][6] << 8 | message_from_server[0][7]

    local_time = time.localtime(reader_time_epoch)
    print("Reader time: " + "{:04d}".format(local_time.tm_year) + "/" + \
        "{:02d}".format(local_time.tm_mon) + "/" + "{:02d}".format(local_time.tm_mday) + \
        " - " + "{:02d}".format(local_time.tm_hour) + ":" + "{:02d}".format(local_time.tm_min) + \
        ":" + "{:02d}".format(local_time.tm_sec) + ":" + "{:03d}".format(reader_time_ms))
    
    msg = binascii.hexlify(message_from_server[0])
    if message_from_server[0][0] == 0 and message_from_server[0][0] == 0:
        print(msg) #00301900017b
        # print("OK")
        # print("size: " + str(msg[16:18]) + "\ndata: " + str(msg[18:]))
        # input()
        # ad91050010a719502b000018
    else:
        print("Error.")
        print(msg[0:4])
        # input()
        
if __name__ == "__main__":
    main()


# 03 81 5e 5d 65 56 00 12



# 00 00 5e 6f d8 63 03 3a
# 03 tags
# 00 ea 
# 
# PC + EPC
# 1800 003015000cfa 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 
# 
# R96
# b0 00 8d d8 5e 82 c7 1e 46 41 b2 8d 
# 
# DATA256
# 68 6f ef 8c 66 bf 9c 69 83 cb b9 17 dc 00 a9 77 81 f0 82 5f 31 62 64 70 a5 b1 1c 26 8b f8 d1 63 
# 
# 
# 1800003015000cfa0000000000000000000000000000000000000000000000000000ea45a3dce616511baa491095ea3f5c71e056f3ad749e2a994f784da98b75956227a2824f86f2476d678c1f28
# 
# 
# 
# 1800003015000cfa00000000000000000000000000000000000000000000000000004314f89a2c0fc02a58ad37085f5d38fde2ffbde610a871431f98d45b36b3916e01d2ed65404805ef5ebd658a