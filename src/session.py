import socket, time

def select_option():
    print("(1) Get current session")
    print("(2) Set session")
    option = int(input())
    if option == 1:
        return 1, "get session"
    elif option == 2:
        session = int(input("Enter the desired session:\n(0) S0\n(1) S1\n(2) S2\n(3) S3\n(4) Dev. Mode\n"))
        if session == 0:
            return 2, "set session s0"
        elif session == 1:
            return 2, "set session s1"
        elif session == 2:
            return 2, "set session s2"
        elif session == 3:
            return 2, "set session s3"
        elif session == 4:
            return 2, "set session dev"
        else:
            exit(-1)
    else:
        exit(-1)

def main():
    command, message_to_server = select_option()
    print("Message to server: " + message_to_server)
    bytes_to_send = str.encode(message_to_server)
    server_addr_port = ("192.168.5.10", 8080)
    buf_size = 1500

    # Create a UDP socket at client side
    udp_client = socket.socket(family=socket.AF_INET, type=socket.SOCK_DGRAM)
    udp_client.settimeout(3)

    # Send to server using created UDP socket
    udp_client.sendto(bytes_to_send, server_addr_port)
    message_from_server = udp_client.recvfrom(buf_size)
    udp_client.close()

    reader_time_epoch = message_from_server[0][2] << 24 | message_from_server[0][3] << 16 \
                        | message_from_server[0][4] << 8 | message_from_server[0][5]

    reader_time_ms = message_from_server[0][6] << 8 | message_from_server[0][7]

    local_time = time.localtime(reader_time_epoch)
    print("Reader time: " + "{:04d}".format(local_time.tm_year) + "/" + \
        "{:02d}".format(local_time.tm_mon) + "/" + "{:02d}".format(local_time.tm_mday) + \
        " - " + "{:02d}".format(local_time.tm_hour) + ":" + "{:02d}".format(local_time.tm_min) + \
        ":" + "{:02d}".format(local_time.tm_sec) + ":" + "{:03d}".format(reader_time_ms))

    if message_from_server[0][0] == 0 and message_from_server[0][0] == 0:
        if command == 1:
            if message_from_server[0][9] == 0x00:
                print("Current session: S0")
            elif message_from_server[0][9] == 0x01:
                print("Current session: S1")
            elif message_from_server[0][9] == 0x02:
                print("Current session: S2")
            elif message_from_server[0][9] == 0x03:
                print("Current session: S3")
            elif message_from_server[0][9] == 0xF0:
                print("Current session: Dev. Mode")
        else:
            print("Session adjusted successfully.")
        input()
    else:
        print("\nError.")
        input()
       
if __name__ == "__main__":
    main()