import socket, time

def main():
    message_to_server = "get version"
    print("Message to server: " + message_to_server)
    bytes_to_send = str.encode(message_to_server)
    server_addr_port = ("192.168.5.10", 8080)
    buf_size = 1500

    # Create a UDP socket at client side
    udp_client = socket.socket(family=socket.AF_INET, type=socket.SOCK_DGRAM)
    udp_client.settimeout(3)

    # Send to server using created UDP socket
    udp_client.sendto(bytes_to_send, server_addr_port)
    message_from_server = udp_client.recvfrom(buf_size)
    udp_client.close()

    reader_time_epoch = message_from_server[0][2] << 24 | message_from_server[0][3] << 16 \
                        | message_from_server[0][4] << 8 | message_from_server[0][5]

    reader_time_ms = message_from_server[0][6] << 8 | message_from_server[0][7]

    local_time = time.localtime(reader_time_epoch)
    print("Reader time: " + "{:04d}".format(local_time.tm_year) + "/" + \
        "{:02d}".format(local_time.tm_mon) + "/" + "{:02d}".format(local_time.tm_mday) + \
        " - " + "{:02d}".format(local_time.tm_hour) + ":" + "{:02d}".format(local_time.tm_min) + \
        ":" + "{:02d}".format(local_time.tm_sec) + ":" + "{:03d}".format(reader_time_ms))

    if message_from_server[0][0] == 0 and message_from_server[0][0] == 0:
        size_fw_rfid = message_from_server[0][8]
        size_fw_digital = message_from_server[0][9]
        start_fw_rfid = 10
        start_fw_digital = 10 + size_fw_rfid
        end_fw_rfid = 10 + size_fw_rfid - 1
        end_fw_digital = 10 + size_fw_rfid + size_fw_digital - 1
        
        fw_rfid = str(message_from_server[0][start_fw_rfid : end_fw_rfid])
        fw_digital = str(message_from_server[0][start_fw_digital : end_fw_digital])
        print("Firmware of the RFID chip: " + fw_rfid[1:])
        print("Firmware of the digital board: " + fw_digital[1:])
        # input()
    else:
        print("\nError.")
        # input()
        
if __name__ == "__main__":
    main()