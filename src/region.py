import socket, time

def select_option():
    print("(1) Get current region")
    print("(2) Set region")
    option = int(input())
    if option == 1:
        return 1, "get region"
    elif option == 2:
        region = int(input("Enter the desired region:\n(0) Korea\n(1) US Wide\n(2) US Narrow\n(3) Europe\n(4) Japan\n(5) China\n(6) Brazil\n"))
        if region == 0:
            return 2, "set region korea"
        elif region == 1:
            return 2, "set region us wide"
        elif region == 2:
            return 2, "set region us narrow"
        elif region == 3:
            return 2, "set region europe"
        elif region == 4:
            return 2, "set region japan"
        elif region == 5:
            return 2, "set region china"
        elif region == 6:
            return 2, "set region brazil"
        else:
            exit(-1)
    else:
        exit(-1)

def main():
    command, message_to_server = select_option()
    print("Message to server: " + message_to_server)
    bytes_to_send = str.encode(message_to_server)
    server_addr_port = ("192.168.5.10", 8080)
    buf_size = 1500

    # Create a UDP socket at client side
    udp_client = socket.socket(family=socket.AF_INET, type=socket.SOCK_DGRAM)
    udp_client.settimeout(3)

    # Send to server using created UDP socket
    udp_client.sendto(bytes_to_send, server_addr_port)
    message_from_server = udp_client.recvfrom(buf_size)
    udp_client.close()

    reader_time_epoch = message_from_server[0][2] << 24 | message_from_server[0][3] << 16 \
                        | message_from_server[0][4] << 8 | message_from_server[0][5]

    reader_time_ms = message_from_server[0][6] << 8 | message_from_server[0][7]

    local_time = time.localtime(reader_time_epoch)
    print("Reader time: " + "{:04d}".format(local_time.tm_year) + "/" + \
        "{:02d}".format(local_time.tm_mon) + "/" + "{:02d}".format(local_time.tm_mday) + \
        " - " + "{:02d}".format(local_time.tm_hour) + ":" + "{:02d}".format(local_time.tm_min) + \
        ":" + "{:02d}".format(local_time.tm_sec) + ":" + "{:03d}".format(reader_time_ms))

    if message_from_server[0][0] == 0 and message_from_server[0][0] == 0:
        if command == 1:
            if message_from_server[0][9] == 0x11:
                print("Current region: Korea")
            elif message_from_server[0][9] == 0x21:
                print("Current region: US Wide")
            elif message_from_server[0][9] == 0x22:
                print("Current region: US Narrow")
            elif message_from_server[0][9] == 0x31:
                print("Current region: Europe")
            elif message_from_server[0][9] == 0x41:
                print("Current region: Japan")
            elif message_from_server[0][9] == 0x52:
                print("Current region: China")
            elif message_from_server[0][9] == 0x61:
                print("Current region: Brazil")
        else:
            print("Region adjusted successfully.")
        input()
    else:
        print("\nError.")
        input()
       
if __name__ == "__main__":
    main()