import socket, time

def select_option():
    print("(1) Set CW on")
    print("(2) Set CW off")
    option = int(input())
    if option == 1:
        return 1, "rf cw on"
    elif option == 2:
        return 2, "rf cw off"
    else:
        exit(-1)

def main():
    command, message_to_server = select_option()
    print("Message to server: " + message_to_server)
    bytes_to_send = str.encode(message_to_server)
    server_addr_port = ("192.168.5.10", 8080)
    buf_size = 1500

    # Create a UDP socket at client side
    udp_client = socket.socket(family=socket.AF_INET, type=socket.SOCK_DGRAM)
    udp_client.settimeout(1)

    # Send to server using created UDP socket
    udp_client.sendto(bytes_to_send, server_addr_port)
    message_from_server = udp_client.recvfrom(buf_size)
    udp_client.close()

    reader_time_epoch = message_from_server[0][2] << 24 | message_from_server[0][3] << 16 \
                        | message_from_server[0][4] << 8 | message_from_server[0][5]

    reader_time_ms = message_from_server[0][6] << 8 | message_from_server[0][7]

    local_time = time.localtime(reader_time_epoch)
    print("Reader time: " + "{:04d}".format(local_time.tm_year) + "/" + \
        "{:02d}".format(local_time.tm_mon) + "/" + "{:02d}".format(local_time.tm_mday) + \
        " - " + "{:02d}".format(local_time.tm_hour) + ":" + "{:02d}".format(local_time.tm_min) + \
        ":" + "{:02d}".format(local_time.tm_sec) + ":" + "{:03d}".format(reader_time_ms))

    if message_from_server[0][0] == 0 and message_from_server[0][0] == 0:
        if command == 1:
            print("CW is on")
        else:
            print("CW is off")
        input()
    else:
        print("\nError.")
        input()
        
if __name__ == "__main__":
    main()