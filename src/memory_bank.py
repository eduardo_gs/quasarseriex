import socket, time
import binascii
# 00571588f8a238b141041180
def select_option():
    print("(0) Read memory")
    print("(1) Write memory")
    print("(2) Lock memory")
    print("(3) Kill memory")
    option = int(input())
    if option == 0:
        read_memory = int(input("Enter the memory bank to be read:\n(0) RFU\n(1) EPC\n(2) TID\n(3) USER\n"))
        
        if read_memory == 0:
            passwd = str(input("Enter the password of the tag (if not, leave it blank): "))
            
            if passwd == "" or passwd == "0":
                passwd = "{:08x}".format(0x00000000)
            elif len(passwd) <= 8: # must have 4 bytes
                passwd = passwd.rjust(8, "0")
            else:
                print("Error: password must be only 4 bytes")
                exit(-1)
            try:
                int(passwd, 16)
            except:
                print("Error: Hexadecimal numbers are only 0 through F")
                exit(-1)

            starting_addr_ptr = str(input("Enter the starting address pointer (if 0, leave it blank): "))
            
            if starting_addr_ptr == "" or starting_addr_ptr == "0":
                starting_addr_ptr = "{:04x}".format(0x0000)
            elif len(starting_addr_ptr) <= 4: # must have 2 bytes
                starting_addr_ptr = starting_addr_ptr.rjust(4, "0")
            else:
                print("Error: starting address pointer must be only 2 bytes")
                exit(-1)
            try:
                int(starting_addr_ptr, 16)
            except:
                print("Error: Hexadecimal numbers are only 0 through F")
                exit(-1)

            data_len = str(input("Enter the data_len (word): "))
            
            if data_len == "" or data_len == "0":
                data_len = "{:04x}".format(0x0000)
            elif len(data_len) <= 4 and len(data_len) > 0: # must have 2 bytes
                data_len = data_len.rjust(4, "0")
            else:
                print("Error: password must be only 2 bytes")
                exit(-1)
            try:
                int(data_len, 16)
            except:
                print("Error: Hexadecimal numbers are only 0 through F")
                exit(-1)
            
            epc = str(input("Enter the EPC of the tag: "))
            if len(epc)%2 != 0 or epc == "":
                print("Error: wrong EPC")
                exit(-1)
            try:
                int(epc, 16)
            except:
                print("Error: Hexadecimal numbers are only 0 through F")
                exit(-1)
            len_epc = str(int(len(epc)/2))
            len_epc = len_epc.rjust(4, "0")
            return 1, "read memory rfu " + passwd + " " + starting_addr_ptr + " " + data_len + " " + len_epc + " " + epc
        
        elif read_memory == 1:
            passwd = str(input("Enter the password of the tag (if not, leave it blank): "))
            
            if passwd == "" or passwd == "0":
                passwd = "{:08x}".format(0x00000000)
            elif len(passwd) <= 8: # must have 32 bytes
                passwd = passwd.rjust(8, "0")
            else:
                print("Error: password must be only 32 bytes")
                exit(-1)
            try:
                int(passwd, 16)
            except:
                print("Error: Hexadecimal numbers are only 0 through F")
                exit(-1)
            
            epc = str(input("Enter the EPC of the tag: "))
            if len(epc)%2 != 0 or epc == "":
                print("Error: wrong EPC")
                exit(-1)
            try:
                int(epc, 16)
            except:
                print("Error: Hexadecimal numbers are only 0 through F")
                exit(-1)
            len_epc = str(int(len(epc)/2))
            return 1, "read memory epc " + passwd + " " + len_epc + " " + epc
        
        elif read_memory == 2:
            passwd = str(input("Enter the password of the tag (if not, leave it blank): "))
            
            if passwd == "" or passwd == "0":
                passwd = "{:08x}".format(0x00000000)
            elif len(passwd) <= 8: # must have 32 bytes
                passwd = passwd.rjust(8, "0")
            else:
                print("Error: password must be only 32 bytes")
                exit(-1)
            try:
                int(passwd, 16)
            except:
                print("Error: Hexadecimal numbers are only 0 through F")
                exit(-1)
            
            epc = str(input("Enter the EPC of the tag: "))
            if len(epc)%2 != 0 or epc == "":
                print("Error: wrong EPC")
                exit(-1)
            try:
                int(epc, 16)
            except:
                print("Error: Hexadecimal numbers are only 0 through F")
                exit(-1)
            len_epc = str(int(len(epc)/2))
            return 1, "read memory tid " + passwd + " " + len_epc + " " + epc
        
        elif read_memory == 3:
            passwd = str(input("Enter the password of the tag (if not, leave it blank): "))
            
            if passwd == "" or passwd == "0":
                passwd = "{:08x}".format(0x00000000)
            elif len(passwd) <= 8: # must have 32 bytes
                passwd = passwd.rjust(8, "0")
            else:
                print("Error: password must be only 32 bytes")
                exit(-1)
            try:
                int(passwd, 16)
            except:
                print("Error: Hexadecimal numbers are only 0 through F")
                exit(-1)
            
            epc = str(input("Enter the EPC of the tag: "))
            if len(epc)%2 != 0 or epc == "":
                print("Error: wrong EPC")
                exit(-1)
            try:
                int(epc, 16)
            except:
                print("Error: Hexadecimal numbers are only 0 through F")
                exit(-1)
            len_epc = str(int(len(epc)/2))
            return 1, "read memory user " + passwd + " " + len_epc + " " + epc
        
        else:
            exit(-1)
    
    elif option == 1:
        pass
    else:
        exit(-1)

def main():
    command, message_to_server = select_option()
    print("Message to server: " + message_to_server)
    bytes_to_send = str.encode(message_to_server)
    server_addr_port = ("192.168.5.10", 8080)
    buf_size = 1500

    # Create a UDP socket at client side
    udp_client = socket.socket(family=socket.AF_INET, type=socket.SOCK_DGRAM)
    udp_client.settimeout(3)

    # Send to server using created UDP socket
    udp_client.sendto(bytes_to_send, server_addr_port)
    message_from_server = udp_client.recvfrom(buf_size)
    udp_client.close()

    reader_time_epoch = message_from_server[0][2] << 24 | message_from_server[0][3] << 16 \
                        | message_from_server[0][4] << 8 | message_from_server[0][5]

    reader_time_ms = message_from_server[0][6] << 8 | message_from_server[0][7]

    local_time = time.localtime(reader_time_epoch)
    print("Reader time: " + "{:04d}".format(local_time.tm_year) + "/" + \
        "{:02d}".format(local_time.tm_mon) + "/" + "{:02d}".format(local_time.tm_mday) + \
        " - " + "{:02d}".format(local_time.tm_hour) + ":" + "{:02d}".format(local_time.tm_min) + \
        ":" + "{:02d}".format(local_time.tm_sec) + ":" + "{:03d}".format(reader_time_ms))

    if message_from_server[0][0] == 0 and message_from_server[0][0] == 0:
        if command == 1:
            msg = binascii.hexlify(message_from_server[0])
            print(msg)
        else:
            print("Region adjusted successfully.")
        input()
    else:
        print("\nError.")
        input()
       
if __name__ == "__main__":
    main()