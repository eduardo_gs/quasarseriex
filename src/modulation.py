import socket, time

def select_option():
    print("(1) Get current modulation mode")
    print("(2) Set modulation mode")
    option = int(input())
    if option == 1:
        return 1, "get modulation"
    elif option == 2:
        modulation = int(input("Select the desired modulation mode: \
                        \n(0) BLF = 40 kHz, RxMode = FM0, DR = 8 \
                        \n(1) BLF = 80 kHz, RxMode = FM0, DR = 8 \
                        \n(2) BLF = 160 kHz, RxMode = FM0, DR = 64/3 \
                        \n(3) BLF = 250 kHz, RxMode = FM0, DR = 64/3 \
                        \n(4) BLF = 320 kHz, RxMode = FM0, DR = 64/3 \
                        \n(5) BLF = 40 kHz, RxMode = M2, DR = 8 \
                        \n(6) BLF = 80 kHz, RxMode = M2, DR = 8 \
                        \n(7) BLF = 160 kHz, RxMode = M2, DR = 64/3 \
                        \n(8) BLF = 250 kHz, RxMode = M2, DR = 64/3 \
                        \n(9) BLF = 320 kHz, RxMode = M2, DR = 64/3 \
                        \n(10) BLF = 40 kHz, RxMode = M4, DR = 8 \
                        \n(11) BLF = 80 kHz, RxMode = M4, DR = 8 \
                        \n(12) BLF = 160 kHz, RxMode = M4, DR = 64/3 \
                        \n(13) BLF = 250 kHz, RxMode = M4, DR = 64/3 \
                        \n(14) BLF = 320 kHz, RxMode = M4, DR = 64/3 \
                        \n(15) BLF = 40 kHz, RxMode = M8, DR = 8 \
                        \n(16) BLF = 80 kHz, RxMode = M8, DR = 8 \
                        \n(17) BLF = 160 kHz, RxMode = M8, DR = 64/3 \
                        \n(18) BLF = 250 kHz, RxMode = M8, DR = 64/3 \
                        \n(19) BLF = 320 kHz, RxMode = M8, DR = 64/3\n"))

        if modulation == 0:
            return 2, "set modulation 40 fm0 8"
        elif modulation == 1:
            return 2, "set modulation 80 fm0 8"
        elif modulation == 2:
            return 2, "set modulation 160 fm0 64/3"
        elif modulation == 3:
            return 2, "set modulation 250 fm0 64/3"
        elif modulation == 4:
            return 2, "set modulation 320 fm0 64/3"
        elif modulation == 5:
            return 2, "set modulation 40 m2 8"
        elif modulation == 6:
            return 2, "set modulation 80 m2 8"
        elif modulation == 7:
            return 2, "set modulation 160 m2 64/3"
        elif modulation == 8:
            return 2, "set modulation 250 m2 64/3"
        elif modulation == 9:
            return 2, "set modulation 320 m2 64/3"
        elif modulation == 10:
            return 2, "set modulation 40 m4 8"
        elif modulation == 11:
            return 2, "set modulation 80 m4 8"
        elif modulation == 12:
            return 2, "set modulation 160 m4 64/3"
        elif modulation == 13:
            return 2, "set modulation 250 m4 64/3"
        elif modulation == 14:
            return 2, "set modulation 320 m4 64/3"
        elif modulation == 15:
            return 2, "set modulation 40 m8 8"
        elif modulation == 16:
            return 2, "set modulation 80 m8 8"
        elif modulation == 17:
            return 2, "set modulation 160 m8 64/3"
        elif modulation == 18:
            return 2, "set modulation 250 m8 64/3"
        elif modulation == 19:
            return 2, "set modulation 320 m8 64/3"
        else:
            exit(-1)
    else:
        exit(-1)

def main():
    command, message_to_server = select_option()
    print("Message to server: " + message_to_server)
    bytes_to_send = str.encode(message_to_server)
    server_addr_port = ("192.168.5.10", 8080)
    buf_size = 1500

    # Create a UDP socket at client side
    udp_client = socket.socket(family=socket.AF_INET, type=socket.SOCK_DGRAM)
    udp_client.settimeout(3)

    # Send to server using created UDP socket
    udp_client.sendto(bytes_to_send, server_addr_port)
    message_from_server = udp_client.recvfrom(buf_size)
    udp_client.close()

    reader_time_epoch = message_from_server[0][2] << 24 | message_from_server[0][3] << 16 \
                        | message_from_server[0][4] << 8 | message_from_server[0][5]

    reader_time_ms = message_from_server[0][6] << 8 | message_from_server[0][7]

    local_time = time.localtime(reader_time_epoch)
    print("Reader time: " + "{:04d}".format(local_time.tm_year) + "/" + \
        "{:02d}".format(local_time.tm_mon) + "/" + "{:02d}".format(local_time.tm_mday) + \
        " - " + "{:02d}".format(local_time.tm_hour) + ":" + "{:02d}".format(local_time.tm_min) + \
        ":" + "{:02d}".format(local_time.tm_sec) + ":" + "{:03d}".format(reader_time_ms))

    if message_from_server[0][0] == 0 and message_from_server[0][0] == 0:
        if command == 1:
            print("Current modulation mode:")
            # valid BLF [9:10]: 0x0028 = 40 KHz,  0x0050 = 80KHz,   0x00A0 = 160 KHz
            #                   0x00FA = 250 KHz, 0x0140 = 320 KHz, 0x0280 = 640 KHz
            # valid RxMod [11]: 0x00 = FM0, 0x01 = M2, 0x02 = M4, 0x03 = M8
            # valid DR [12]: 0x00 = 8, 0x01 = 64/3
            if message_from_server[0][9] << 8 | message_from_server[0][10] == 0x0028:
                print("BLF = 40 kHz")
            elif message_from_server[0][9] << 8 | message_from_server[0][10] == 0x0050:
                print("BLF = 80 kHz")
            elif message_from_server[0][9] << 8 | message_from_server[0][10] == 0x00A0:
                print("BLF = 160 kHz")
            elif message_from_server[0][9] << 8 | message_from_server[0][10] == 0x00FA:
                print("BLF = 250 kHz")
            elif message_from_server[0][9] << 8 | message_from_server[0][10] == 0x0140:
                print("BLF = 320 kHz")
            elif message_from_server[0][9] << 8 | message_from_server[0][10] == 0x0280:
                print("BLF = 640 kHz")
            
            if message_from_server[0][11] == 0x00:
                print("RxMod = FM0")
            elif message_from_server[0][11] == 0x01:
                print("RxMod = M2")
            elif message_from_server[0][11] == 0x02:
                print("RxMod = M4")
            elif message_from_server[0][11] == 0x03:
                print("RxMod = M8")

            if message_from_server[0][12] == 0x00:
                print("DR = 8")
            elif message_from_server[0][12] == 0x01:
                print("DR = 64/3")
        else:
            print("Modulation mode adjusted successfully.")
        input()
    else:
        print("\nError.")
        input()
       
if __name__ == "__main__":
    main()