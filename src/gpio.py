import socket, time

def select_option():
    print("(0) Set the GPIO as input")
    print("(1) Set the GPIO as output")
    print("(3) Write 0x00 to the GPIO")
    print("(4) Write 0x01 to the GPIO")
    print("(5) Read the GPIO")
    option = int(input())
    if option == 0:
        gpio = int(input("Enter the desired GPIO (1 to 8): "))
        return 0, "ckeck antenna"
    elif option == 1:
        gpio = int(input("Enter the desired GPIO (1 to 8): "))
        return 1, "get antenna"
    elif option == 2:
        gpio = int(input("Enter the desired GPIO (1 to 8): "))
        return 2, "set antenna " + "{:02d}".format(sel_antenna)
    elif option == 3:
        gpio = int(input("Enter the desired GPIO (1 to 8): "))
        return 1, "get antenna"
    elif option == 4:
        gpio = int(input("Enter the desired GPIO (1 to 8): "))
        return 1, "get antenna"
    elif option == 5:
        gpio = int(input("Enter the desired GPIO (1 to 16): "))
        return 1, "get antenna"
    else:
        exit(-1)

def main():
    command, message_to_server = select_option()
    print("Message to server: " + message_to_server)
    bytes_to_send = str.encode(message_to_server)
    server_addr_port = ("192.168.5.10", 8080)
    buf_size = 1500

    # Create a UDP socket at client side
    udp_client = socket.socket(family=socket.AF_INET, type=socket.SOCK_DGRAM)
    udp_client.settimeout(1)

    # Send to server using created UDP socket
    udp_client.sendto(bytes_to_send, server_addr_port)
    message_from_server = udp_client.recvfrom(buf_size)
    udp_client.close()

    reader_time_epoch = message_from_server[0][2] << 24 | message_from_server[0][3] << 16 \
                        | message_from_server[0][4] << 8 | message_from_server[0][5]

    reader_time_ms = message_from_server[0][6] << 8 | message_from_server[0][7]

    local_time = time.localtime(reader_time_epoch)
    print("Reader time: " + "{:04d}".format(local_time.tm_year) + "/" + \
        "{:02d}".format(local_time.tm_mon) + "/" + "{:02d}".format(local_time.tm_mday) + \
        " - " + "{:02d}".format(local_time.tm_hour) + ":" + "{:02d}".format(local_time.tm_min) + \
        ":" + "{:02d}".format(local_time.tm_sec) + ":" + "{:03d}".format(reader_time_ms))

    if message_from_server[0][0] == 0 and message_from_server[0][0] == 0:
        if command == 0:
            print("The antenna is normal")
        elif command == 1:
            if message_from_server[0][8] == 0:
                model = "PoS"
            elif message_from_server[0][8] == 1:
                model = "RFID reader with output for 2 antennas"
            elif message_from_server[0][8] == 2:
                model = "RFID reader with output for 4 antennas"
            elif message_from_server[0][8] == 3:
                model = "RFID reader with output for 8 antennas"
            elif message_from_server[0][8] == 4:
                model = "RFID reader with output for 16 antennas"
                
            current_antenna = str(int(message_from_server[0][9]))
            print("Model: " + model + ".\nCurrent antenna: " + current_antenna + ".")
        else:
            print("Antenna successfully selected")
        input()
    else:
        print("\nError.")
        input()
        
if __name__ == "__main__":
    main()