import socket, time

def select_option():
    print("(1) Get current anti-collision mode")
    print("(2) Set anti-collision dynamic mode")
    print("(3) Set anti-collision fixed mode")
    option = int(input())
    if option == 1:
        return 1, "get anti-collision"
    elif option == 2:
        q_start = str(float(input("Enter the desired q start (0 to 15): ")))
        q_max = str(float(input("Enter the desired q max (0 to 15): ")))
        q_min = str(float(input("Enter the desired q min (0 to 15): ")))
        return 2, "set anti-collision dynamic " + q_start + q_max + q_min
    elif option == 3:
        q_start = str(float(input("Enter the desired q start (0 to 15): ")))
        q_max = str(float(input("Enter the desired q max (0 to 15): ")))
        q_min = str(float(input("Enter the desired q min (0 to 15): ")))
        return 3, "set anti-collision fixed " + q_start + q_max + q_min
    else:
        exit(-1)

def main():
    command, message_to_server = select_option()
    print("Message to server: " + message_to_server)
    bytes_to_send = str.encode(message_to_server)
    server_addr_port = ("192.168.5.10", 8080)
    buf_size = 1500

    # Create a UDP socket at client side
    udp_client = socket.socket(family=socket.AF_INET, type=socket.SOCK_DGRAM)
    udp_client.settimeout(3)

    # Send to server using created UDP socket
    udp_client.sendto(bytes_to_send, server_addr_port)
    message_from_server = udp_client.recvfrom(buf_size)
    udp_client.close()

    reader_time_epoch = message_from_server[0][2] << 24 | message_from_server[0][3] << 16 \
                        | message_from_server[0][4] << 8 | message_from_server[0][5]

    reader_time_ms = message_from_server[0][6] << 8 | message_from_server[0][7]

    local_time = time.localtime(reader_time_epoch)
    print("Reader time: " + "{:04d}".format(local_time.tm_year) + "/" + \
        "{:02d}".format(local_time.tm_mon) + "/" + "{:02d}".format(local_time.tm_mday) + \
        " - " + "{:02d}".format(local_time.tm_hour) + ":" + "{:02d}".format(local_time.tm_min) + \
        ":" + "{:02d}".format(local_time.tm_sec) + ":" + "{:03d}".format(reader_time_ms))

    if message_from_server[0][0] == 0 and message_from_server[0][0] == 0:
        if command == 1:
            mode = ""
            if message_from_server[0][9] == 0x00:
                mode = "fixed"
            else:
                mode = "dynamic"
            q_start = str(int(message_from_server[0][10]))
            q_max = str(int(message_from_server[0][11]))
            q_min = str(int(message_from_server[0][12]))
            print("Current anti-collision mode: " + mode + ". q_start = " + q_start + ". q_max = " + q_max + ". q_min = " + q_min)
        else:
            print("Anti-collision mode adjusted successfully.")
        input()
    else:
        print("\nError.")
        input()
       
if __name__ == "__main__":
    main()